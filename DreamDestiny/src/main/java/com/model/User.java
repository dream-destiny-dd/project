package com.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User {
	
	@Id@GeneratedValue
	private  int userId;
	private String firstName;
	private String lastName;
	private String gender;
	private  Date  dateOfBirth;
	private String phoneNumber;
	private String emailId;
	private String address;
	private String city;
	private String state;
	private  long  pinCode;
	private String password;
	private String otp;
	
	public User(){
		
	}
	
	public User(int userId, String firstName, String lastName, String gender, Date dateOfBirth, String phoneNumber,
			String emailId, String address, String city, String state, long pinCode , String password) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.dateOfBirth = dateOfBirth;
		this.phoneNumber = phoneNumber;
		this.emailId = emailId;
		this.address = address;
		this.city = city;
		this.state = state;
		this.pinCode = pinCode;
		this.password=password;
	}
	
	public int getUserId() {
		return userId;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public long getPinCode() {
		return pinCode;
	}
	
	public void setPinCode(long pinCode) {
		this.pinCode = pinCode;
	}
	
	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



	@Override
	public String toString() {
		return "User [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", gender=" + gender
				+ ", dateOfBirth=" + dateOfBirth + ", phoneNumber=" + phoneNumber + ", emailId=" + emailId
				+ ", address=" + address + ", city=" + city + ", state=" + state + ", pinCode=" + pinCode + ", otp="
				+  "]";
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}
}