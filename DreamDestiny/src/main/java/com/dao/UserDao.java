package com.dao;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.api.v2010.account.Message;

import com.model.User;

@Service
public class UserDao {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private JavaMailSender mailSender;
	
	private static final String ACCOUNT_SID = "AC58dd1ee94fa896a70db435582666d2c4";
    private static final String AUTH_TOKEN = "7628e5e5d84a002767a2b636eb1d669d";
    private static final String TWILIO_PHONE_NUMBER = "+19703663980";

	private static final int OTP_LENGTH = 6;
	
	static {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    }

	public List<User> getUsers() {
	 return userRepository.findAll();
	}

	public User getUserById(int userId) {
		User user=new User();
		return userRepository.findById(userId).orElse(user);
	}

	public List<User> getUserByName(String lastName) {
		return userRepository.findByName(lastName) ;
	}
	
	public User Login(String emailId, String password) {
		User user = userRepository.findByEmailId(emailId);

		if (user != null) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			if (passwordEncoder.matches(password, user.getPassword())) {
				return user;
			}
		}

		return null;
    }
	
//	public User userLogin(String emailId, String password) {
//        User user = userRepository.userLogin(emailId, password);
//
//        return user;
//    }
	private String generateOTP() {
        Random random = new Random();
        StringBuilder otp = new StringBuilder();

        for (int i = 0; i < OTP_LENGTH; i++) {
            otp.append(random.nextInt(10));
        }

        return otp.toString();
    }
	
	// Add employee with OTP generation
    public User addUser(User user) {
        BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
        String encryptedPwd = bcrypt.encode(user.getPassword());
        user.setPassword(encryptedPwd);

        // Generate OTP
        String otp = generateOTP();
        user.setOtp(otp);

        // Save the employee
        User savedUser = userRepository.save(user);


        // Send OTP via SMS using Twilio
        sendOtpViaSms(savedUser);
        sendWelcomeEmail(savedUser);
        generateAndSendOtp(savedUser);


        return savedUser;
    }
    
    private void sendWelcomeEmail(User user) {
		
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(user.getEmailId());
		message.setSubject("Welcome to our website");
		message.setText("Dear " + user.getLastName() + ",\n\n"
				+ "Thank you for registering ");

		mailSender.send(message);
	}
    
    //To generate OTP FOR MAIL---
    private void generateAndSendOtp(User user) {
        String otp = generateOTP();
        user.setOtp(otp);
        
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(user.getEmailId());
        message.setSubject("Your OTP for registration");
        message.setText("Your OTP is: " + otp);

        mailSender.send(message);
    }

	// Send OTP via SMS using Twilio
    private void sendOtpViaSms(User user) {
        try {
            Message message = Message.creator(
                    new com.twilio.type.PhoneNumber(user.getPhoneNumber()),
                    new com.twilio.type.PhoneNumber(TWILIO_PHONE_NUMBER),
                    "Your OTP for registration is: " + user.getOtp())
                    .create();

            System.out.println("OTP sent successfully via SMS.");
        } catch (ApiException e) {
            if (e.getCode() == 21614) {
                // Twilio error code 21614 corresponds to "Trial accounts cannot send messages to unverified numbers"
                System.err.println("OTP not sent: Twilio trial accounts cannot send messages to unverified numbers.");
            } else {
                System.err.println("Error sending OTP via SMS: " + e.getMessage());
            }
        }
    }

//	public User addUser(User user) {
//		return userRepository.save(user);
//	}

	public User updateUser(User user) {
		return userRepository.save(user);
	}

	public void deleteUserById(int userId) {
		userRepository.deleteById(userId);
	}

}