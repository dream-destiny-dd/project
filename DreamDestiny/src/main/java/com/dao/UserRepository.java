package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.model.User;

@Repository
public interface UserRepository  extends JpaRepository<User, Integer>{

	@Query("from User where lastName = :Name")
	List<User> findByName(@Param("Name") String productName);
	
	@Query("from User where emailId = :emailId and password =:password")
	User userLogin(@Param("emailId") String emailId,@Param("password") String password);

	@Query("from User where emailId = :emailId")
	User findByEmailId(@Param("emailId") String emailId );

}