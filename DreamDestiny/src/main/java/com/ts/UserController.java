package com.ts;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDao;
import com.model.User;

@RestController
@CrossOrigin
public class UserController {

	 //Implementing Dependency Injection for UserDao 
		@Autowired
		UserDao userDao;
		
		@GetMapping("getAllUsers")
		public List<User> getUser() {		
			return userDao.getUsers();
		}
		
		@GetMapping("getUserById/{userId}")
			public User getUserById(@PathVariable("userId") int userId){
			 return userDao.getUserById(userId);
		}
		
		@GetMapping("getUserByName/{lastName}")
		public List<User> getProductByName(@PathVariable("lastName") String lastName) {
			return userDao.getUserByName(lastName);
		}
		
		@GetMapping("userLogin/{emailId}/{password}")
		public User userLogin(@PathVariable String emailId, @PathVariable String password) {
			return userDao.Login(emailId, password);
		}
		
		@PostMapping("addUser")
		public User addUser(@RequestBody User user){
			return userDao.addUser(user);
		}
		
		@PutMapping("UpdateUser")
		public User updateUser(@RequestBody User user){
			return userDao.updateUser(user);
		}
		
		@DeleteMapping("deleteEmployeeById/{userId}")
		public String deleteEmployeeById(@PathVariable int userId){
			userDao.deleteUserById(userId);
			return "user with userId : " + userId + " Deleted Successfully " ;
		}
		
}