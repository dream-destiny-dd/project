import { Component } from '@angular/core';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrl: './places.component.css'
})
export class PlacesComponent {

  couples: any;
  friends:any;
  emailId: any;
  cartItems: any;  //AddToCart

  constructor() {
    //AddToCart
    this.cartItems = [];
    // this.emailId = localStorage.getItem('emailId');

    this.couples = [
      {
        id: 1001,
        name: "DARJEELING",
        flipname: "A Love Affair with the Hills",
        description: "Darjeeling offers a romantic escape with its stunning vistas and tranquil ambiance. Enjoy a sunrise at Tiger Hill, painting the sky with hues of pink and orange, or take a leisurely stroll through the lush tea gardens, hand in hand. Indulge in  a romantic meal with panoramic views of the mountains. perfect destination for couples seeking a romantic retreat amidst nature's beauty.",
        imgsrc: "assets/images/darjeeling.jpg"
      },

      { 
        id: 1002,
        name: "TAJ MAHAL", 
        flipname: "A lover's tale in stone",
        description: "An architectural marvel in Agra, India, is a symbol of eternal love. It stands as a testament to their love story. Visiting at sunrise or sunset offers a breathtaking view of its white marble facade glowing in the changing light, creating a romantic ambiance. Walking through its gardens and admiring its intricate details, one can't help but feel the enduring love that inspired its creation.", 
        imgsrc: "assets/images/taj.avif" 
      },

      { 
        id: 1003, 
        name: "WAYANAD", 
        flipname:"Enchanting Escapes for Two Hearts",
        description: "Wayanad, a serene district nestled in the Western Ghats of Kerala, India, offers couples a magical escape into nature's lap. With its lush greenery, misty mountains, cascading waterfalls, and rich biodiversity, Wayanad is an idyllic destination for romantic getaways.", 
        imgsrc: "assets/images/wayanad.jpg" 
      },

      { 
        id: 1004, 
        name: "ANDAMAN AND NICOBAR ISLANDS", 
        flipname: "Love Paradise",
        description: "Nestled in the lap of the Bay of Bengal, With their pristine beaches, azure waters, and lush greenery, these islands offer the perfect backdrop for a romantic getaway. Walk along the shore hand in hand, indulge in a candlelit dinner by the beach, and let the magic of the islands kindle the flames of love", 
        imgsrc: "assets/images/andaman.jpg" 
      },

      { 
        id: 1005, 
        name: "MANALI", 
        flipname: "A Symphony of Love",
        description: "Manali is a haven for lovers seeking a romantic escape. From its snow-capped peaks to its lush valleys, every corner of Manali is a canvas painted with love. Walk hand in hand through its quaint streets, breathe in the crisp mountain air, and let the magic of Manali weave a tale of romance that will linger in your hearts forever.", 
        imgsrc: "assets/images/manali.jpg"  
      },

      { 
        id: 1006, 
        name: "SHIMLA", 
        flipname:"Where Love Finds its Mountainous Bliss",
        description: "Shimla, the capital city of Himachal Pradesh in northern India, is a charming hill station nestled amidst the snow-capped Himalayan ranges. Renowned for its picturesque landscapes, colonial architecture, and pleasant climate, Shimla is often referred to as the Queen of Hill Stations.", 
        imgsrc: "assets/images/shimla.jpg" 
      },

      { 
        id: 1007, 
        name: "COORG",
        flipname:"Where Nature's Symphony Serenades the Soul", 
        description: "Coorg, also known as Kodagu, is a mesmerizing hill station nestled in the Western Ghats of Karnataka, India. Renowned for its verdant coffee plantations, misty mountains, cascading waterfalls, and rich cultural heritage.", 
        imgsrc: "assets/images/coorg.avif" 
      },

      { 
        id: 1008, 
        name: "ALLEPPEY", 
        flipname: "Where Love Sets Sail on Serene Waters",
        description: "Alleppey, also known as Alappuzha, is a picturesque town located in the southern Indian state of Kerala. Renowned for its enchanting backwaters, serene beaches, lush greenery, and rich cultural heritage, Alleppey is often referred to as the Venice of the East.", 
        imgsrc: "assets/images/alleppey.avif" 
      },

      { 
        id: 1009, 
        name: "LONAVALA", 
        flipname:"Where Every Moment is a Hillside Retreat",
        description: "Lonavala captivates couples with its scenic beauty, serene ambiance, and myriad experiences, making it an enchanting hillside retreat that beckons lovers to escape into a world of romance and tranquility amidst the splendor of nature.", 
        imgsrc: "assets/images/lonavala.jpg" 
      },
    ];

    this.friends = [
      {
        id: 1000,
        name: "GOA",
        flipname: "Where Every Moment is an Epic Adventure with Friends",
        description: "Goa, a coastal paradise on the western shores of India, is synonymous with sun, sand, and endless fun, making it the ultimate destination for unforgettable adventures with friends. From vibrant beaches to lively nightlife,",
        imgsrc: "assets/images/goa.jpg"
      },

      { 
        id: 1001,
        name: "PONDICHERRY", 
        flipname: "Unforgettable Moments, Endless Memories with Friends",
        description: "Pondicherry, a charming coastal town on the southeastern coast of India, beckons friends with its unique blend of French colonial heritage, vibrant culture, and serene beaches. Pondicherry offers the perfect setting for friends to unwind, explore, and create unforgettable memories together.",
        imgsrc: "assets/images/pondicherry.webp" 
      },

      { 
        id: 1002, 
        name: "KERALA", 
        flipname: "God's Own Country",
        description: "From exploring misty hill stations to cruising through tranquil backwaters and indulging in mouthwatering cuisine, Kerala offers friends the perfect setting to bond, explore, and create cherished memories together.",        
        imgsrc: "assets/images/kerala.webp" 
      },

      { 
        id: 1003, 
        name: "HYDERABAD", 
        flipname:"Where biryani bonds people", 
        description: " At the heart of the city lies the iconic Charminar, a symbol of Hyderabad's rich heritage and architectural splendor. Surrounding this historic monument, the bustling lanes of the old city beckon with their colorful markets, where friends can immerse themselves in the sights, sounds, and aromas of traditional bazaars.",        
        imgsrc: "assets/images/hyd.webp" 
      },

      { 
        id: 1004, 
        name: "GOKARNA", 
        flipname: "Where Every Wave is a Melody of Friendship",
        description: "Nestled along the picturesque coastline of Karnataka, Gokarna is a hidden gem that beckons friends with its pristine beaches, rugged cliffs, and laid-back vibe. From beach hopping to water sports and beachside bonfires.",        
        imgsrc: "assets/images/gokarna.webp" 
      },

      { 
        id: 1005, 
        name: "RISHIKESH", 
        flipname:"Where Every Beat of Adventure Echoes Friendship",
        description: " From thrilling river rafting to serene yoga sessions and vibrant street markets, Rishikesh offers friends the perfect blend of adventure, spirituality, and camaraderie. For the thrill-seekers, Rishikesh is a paradise of adventure activities waiting to be explored. ",        
        imgsrc: "assets/images/rishikesh.jpg" 
      },

      { 
        id: 1006, 
        name: "RAJASTHAN", 
        flipname:"Where Every Sunset Paints a Canvas of Friendship",
        description: "Rajasthan, the Land of Kings, is a vibrant tapestry of colors, cultures, and majestic landscapes that beckons friends with its regal charm, timeless heritage, and warm hospitality. From exploring ancient forts and palaces to camel safaris in the desert and vibrant bazaars.",
        imgsrc: "assets/images/rajasthan.jpg" 
      },

      { 
        id: 1007, 
        name: "LADAKH",
        flipname:"Where Friendship Reaches New Heights", 
        description: "Embark on a thrilling road trip along the scenic highways of Ladakh, where friends can traverse winding mountain roads, cross high-altitude passes, and marvel at breathtaking vistas of snow-capped peaks and azure lakes. ",        
        imgsrc: "assets/images/ladakh.jpg" 
      },

      { 
        id: 1008, 
        name: "SIKKIM", 
        flipname:"Where Nature's Symphony Serenades the Soul", 
        description: "Indulge in the flavors of Sikkimese cuisine, with its unique blend of Tibetan, Nepali, and Bhutanese influences. Share meals of momos, thukpa, and gundruk soup at local eateries and homestays, savoring the rich flavors and spices of traditional Sikkimese dishes.",        
        imgsrc: "assets/images/sikkim.jpg" 
      },

      { 
        id: 1009, 
        name: "WEST BENGAL", 
        flipname:"Where Tradition Meets Modernity", 
        description: "Experience the vibrant culture and traditions of West Bengal by attending colorful festivals and cultural events. Join locals in celebrating festivals like Durga Puja, Diwali, and Holi, where streets come alive with music, dance, and elaborate decorations.",        
        imgsrc: "assets/images/westbengal.webp"
      },

      { 
        id: 1010, 
        name: "MEGHALAYA", 
        flipname:"Where Friendship Blooms Amid Nature's Embrace", 
        description: " From exploring ancient caves to wandering through lush rainforests and soaking in the beauty of cascading waterfalls, Meghalaya promises friends an unforgettable journey of discovery and camaraderie.",        
        imgsrc: "assets/images/meghalaya.avif" 
      },

      
    ];


  }

  ngOnInit() {
  }

  addToCart(product: any) {
    this.cartItems.push(product);
    localStorage.setItem("cartItems", JSON.stringify(this.cartItems));
  }

}
