import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-signout',
  templateUrl: './signout.component.html',
  styleUrls: ['./signout.component.css']
})
export class SignoutComponent implements OnInit {
  constructor(private router: Router, private service: UserService){

    
    // localStorage.removeItem('emailId');
    // localStorage.clear();

    //Setting the isUserLoggedIn variable value to false under EmpService
    this.service.setIsUserLoggedOut();

    alert("Successfully LoggedOut");
    this.router.navigate(['signin']);
  }
  ngOnInit(){
  }
}