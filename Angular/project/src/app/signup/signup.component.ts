import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrl: './signup.component.css'
})
export class SignupComponent implements OnInit{

  siteKey: string = '6LcVAGgpAAAAADQhiqlMJk7-mGv-eBqLDx2D4rAo'; // Add your reCAPTCHA site key here
  aFormGroup! : FormGroup;
  user:any;
  formData: any = {
    firstName:'',
    lastName:'',
    gender:'',
    dateOfBirth:'',
    phoneNumber:'',
    emailId:'',
    address:'',
    city:'',
    state:'',
    pinCode:'',
    password: '', // Add password property with initial value
    confirmPassword: '', // Add confirmPassword property with initial value
  };

  constructor(private toastr: ToastrService,private service:UserService,private router:Router) { }
  ngOnInit(){
    this.aFormGroup = new FormGroup({
      // Add other form controls here
      recaptcha: new FormControl('', Validators.required)
    });
  }

  onSubmit(formData: any) {
    const firstName=formData.firstName;
    const lastName=formData.lastName;
    const gender=formData.gender;
    const dateOfBirth=formData.dateOfBirth;
    const phoneNumber=formData.phoneNumber;
    const emailId=formData.emailId;
    const address=formData.address;
    const state=formData.state;
    const city=formData.city;
    const pinCode=formData.pinCode;
    const password = formData.password;
    const confirmPassword = formData.confirmPassword;
    console.log(password);
    console.log(confirmPassword);

    if(password == confirmPassword){
      // console.log(formData);
      this.toastr.success('Registration Successful!');
      this.service. registerUser(formData).subscribe((data: any) => { console.log(data); });
      //  this.router.navigate(['login']);
    }else {
      console.log("Error");
    }

  }
}
