import { Component } from '@angular/core';

@Component({
  selector: 'app-family',
  templateUrl: './family.component.html',
  styleUrl: './family.component.css'
})
export class FamilyComponent {

  families: any;

  constructor() {

    this.families = [
      {
        id:1001, 
        name:"ADIYOGI",   
        flipname:"Embrace the Divine Within. Adiyogi: The Source of Yoga.", 
        description:"Adiyogi statue is a breathtaking symbol of spirituality and inner transformation. Located in the sacred space of the Isha Yoga Center in Coimbatore, India, this magnificent sculpture represents the first yogi, Adiyogi, who is believed to have imparted the wisdom of yoga to humanity thousands of years ago.", 
        imgsrc:"assets/images/adiyogii.jpg"
      },

      {
        id:1002, 
        name:"TIRUPATI", 
        flipname:"Beneath the Divine Sky: Tirupati, Where Faith Meets Serenity.", 
        description:"Nestled amidst the lush greenery of the Eastern Ghats in the state of Andhra Pradesh, Tirupati is a sacred haven for millions of devotees seeking solace and spiritual fulfillment. At its heart lies the revered Sri Venkateswara Temple, a symbol of devotion and architectural grandeur.", 
        imgsrc:"assets/images/tirupati.jpg"
      },

      {
        id:1003, 
        name:"SHIRDI",  
        flipname:"Sacred Shirdi: Where Faith Flourishes.", 
        description:"At the heart of Shirdi lies the majestic shrine of Sai Baba, a place of pilgrimage and reverence. Devotees from all corners of the globe gather here to pay homage to the saint, whose teachings of love, compassion, and selfless service continue to inspire generations.", 
        imgsrc:"assets/images/shirdi.jpg"
      },

      {
        id:1004, 
        name:"VARANASI",  
        flipname:"Where Timeless Tradition Meets Spiritual Splendor.", 
        description:"Nestled along the banks of the sacred Ganges River in Uttar Pradesh, India, Varanasi, also known as Kashi, is one of the world's oldest continuously inhabited cities and a beacon of spiritual enlightenment.", 
        imgsrc:"assets/images/varanasi.jpg"
      },

      {
        id:1001, 
        name:"BHADRACHALAM",   
        flipname:"Where Devotion Meets Divine Grace.", 
        description:"Nestled on the banks of the majestic Godavari River in the state of Telangana, India, Bhadrachalam is a tranquil town steeped in spiritual significance and revered as the abode of Lord Rama.", 
        imgsrc:"assets/images/bhadrachalam.jpg"
      },

      {
        id:1002, 
        name:"Goa", 
        flipname:24999.00, 
        description:"No Cost EMI Applicable", 
        imgsrc:"assets/Images/goa2.jpg"
      },

      {
        id:1003, 
        name:"Kerala",  
        flipname:34999.00, 
        description:"No Cost EMI Applicable", 
        imgsrc:"assets/Images/kerala1.jpg"
      },

      {
        id:1004, 
        name:"Kerala",  
        flipname:44999.00, 
        description:"No Cost EMI Applicable", 
        imgsrc:"assets/Images/kerala2.jpg"
      },
    ];
  }
}
