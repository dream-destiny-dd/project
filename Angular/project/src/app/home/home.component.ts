import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit {
  flipped: boolean = false;
  constructor(private router:Router){

  }

  quotes: string[] = [
    "“Remember that happiness is a way of travel – not a destination.”",
    "“We live in a wonderful world that is full of beauty, charm and adventure. There is no end to the adventures we can have if only we seek them with our eyes open.”",
    "“Life is either a daring adventure or nothing at all.”",
    "“Live life with no excuses, travel with no regret.”",
    "“It is only in adventure that some people succeed in knowing themselves – in finding themselves.”",
    "“Tourists don’t know where they’ve been, travelers don’t know where they’re going.” ",
    "“A good traveler has no fixed plans, and is not intent on arriving.” ",
    "“There are no foreign lands. It is the traveler only who is foreign.”",
    "“Life is meant for good friends and great adventures.”",
    "“Once a year, go somewhere you have never been before.” "
  ];

  randomQuote: string = '';


  ngOnInit(): void {
    this.getRandomQuote();
  }

  getRandomQuote() {
    this.randomQuote = this.quotes[Math.floor(Math.random() * this.quotes.length)];
  }
  wander(){
    this.router.navigate(['/friends']); 
  }
  fly(){
    this.router.navigate(['/couple']); 
  }

  explore(){
    this.router.navigate(['/family']); 
  }
}
