import { Component } from '@angular/core';
import { UserService } from '../user.service';

interface Couple {
  id: number;
  name: string;
  flipname: string;
  description: string;
  imgsrc: string;
}

@Component({
  selector: 'app-couple',
  templateUrl: './couple.component.html',
  styleUrl: './couple.component.css'
})
export class CoupleComponent {

  couples: Couple[];



  constructor(private service: UserService) {
  this.couples = [
    {
      id: 1001,
      name: "DARJEELING",
      flipname: "A Love Affair with the Hills",
      description: "Darjeeling offers a romantic escape with its stunning vistas and tranquil ambiance. Enjoy a sunrise at Tiger Hill, painting the sky with hues of pink and orange, or take a leisurely stroll through the lush tea gardens, hand in hand. Indulge in  a romantic meal with panoramic views of the mountains. perfect destination for couples seeking a romantic retreat amidst nature's beauty.",
      imgsrc: "assets/images/darjeeling.jpg"
    },

    { 
      id: 1002,
      name: "TAJ MAHAL", 
      flipname: "A lover's tale in stone",
      description: "An architectural marvel in Agra, India, is a symbol of eternal love. It stands as a testament to their love story. Visiting at sunrise or sunset offers a breathtaking view of its white marble facade glowing in the changing light, creating a romantic ambiance. Walking through its gardens and admiring its intricate details, one can't help but feel the enduring love that inspired its creation.", 
      imgsrc: "assets/images/taj.avif" 
    },

    { 
      id: 1003, 
      name: "WAYANAD", 
      flipname:"Enchanting Escapes for Two Hearts",
      description: "Wayanad, a serene district nestled in the Western Ghats of Kerala, India, offers couples a magical escape into nature's lap. With its lush greenery, misty mountains, cascading waterfalls, and rich biodiversity, Wayanad is an idyllic destination for romantic getaways.", 
      imgsrc: "assets/images/wayanad.jpg" 
    },

    { 
      id: 1004, 
      name: "ANDAMAN AND NICOBAR ISLANDS", 
      flipname: "Love Paradise",
      description: "Nestled in the lap of the Bay of Bengal, With their pristine beaches, azure waters, and lush greenery, these islands offer the perfect backdrop for a romantic getaway. Walk along the shore hand in hand, indulge in a candlelit dinner by the beach, and let the magic of the islands kindle the flames of love", 
      imgsrc: "assets/images/andaman.jpg" 
    },

    { 
      id: 1005, 
      name: "MANALI", 
      flipname: "A Symphony of Love",
      description: "Manali is a haven for lovers seeking a romantic escape. From its snow-capped peaks to its lush valleys, every corner of Manali is a canvas painted with love. Walk hand in hand through its quaint streets, breathe in the crisp mountain air, and let the magic of Manali weave a tale of romance that will linger in your hearts forever.", 
      imgsrc: "assets/images/manali.jpg"  
    },

    { 
      id: 1006, 
      name: "SHIMLA", 
      flipname:"Where Love Finds its Mountainous Bliss",
      description: "Shimla, the capital city of Himachal Pradesh in northern India, is a charming hill station nestled amidst the snow-capped Himalayan ranges. Renowned for its picturesque landscapes, colonial architecture, and pleasant climate, Shimla is often referred to as the Queen of Hill Stations.", 
      imgsrc: "assets/images/shimla.jpg" 
    },

    { 
      id: 1007, 
      name: "COORG",
      flipname:"Where Nature's Symphony Serenades the Soul", 
      description: "Coorg, also known as Kodagu, is a mesmerizing hill station nestled in the Western Ghats of Karnataka, India. Renowned for its verdant coffee plantations, misty mountains, cascading waterfalls, and rich cultural heritage.", 
      imgsrc: "assets/images/coorg.avif" 
    },

    { 
      id: 1008, 
      name: "ALLEPPEY", 
      flipname: "Where Love Sets Sail on Serene Waters",
      description: "Alleppey, also known as Alappuzha, is a picturesque town located in the southern Indian state of Kerala. Renowned for its enchanting backwaters, serene beaches, lush greenery, and rich cultural heritage, Alleppey is often referred to as the Venice of the East.", 
      imgsrc: "assets/images/alleppey.avif" 
    },

    { 
      id: 1009, 
      name: "LONAVALA", 
      flipname:"Where Every Moment is a Hillside Retreat",
      description: "Lonavala captivates couples with its scenic beauty, serene ambiance, and myriad experiences, making it an enchanting hillside retreat that beckons lovers to escape into a world of romance and tranquility amidst the splendor of nature.", 
      imgsrc: "assets/images/lonavala.jpg" 
    },
  ];
}

addToCart(couple: any){
  this.service.addToCart(couple);
}
}