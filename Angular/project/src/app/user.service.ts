import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  cartItems: any;
  loginStatus: any;
  isUserLoggedIn: any;

  constructor(private http:HttpClient) { 
    this.loginStatus = new Subject();
    this.isUserLoggedIn = false;

    this.cartItems = [];

  }
  registerUser(user:any): any {
    return this.http.post('http://localhost:8085/addUser',user);
  }

  userLogin(emailId: any, password: any): any {
    return this.http.get('http://localhost:8085/userLogin/' + emailId + '/' + password).toPromise();
  }

  addToCart(couple: any) {
    this.cartItems.push (couple);
  }

  getCartItems() {
    return this.cartItems;
  }
  setCartItems() {
    this.cartItems.splice();
  }

  
  //login
  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }

  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }
  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }
}
