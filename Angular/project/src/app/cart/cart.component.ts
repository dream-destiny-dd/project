import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css'
})
export class CartComponent implements OnInit {
  // cartItems: any;
  // cartEvents: any;
  localStorageData: any;
  email: any;
  total: number;
  couples: any;

  constructor(private service: UserService) {
    this.total = 0;
    this.email = localStorage.getItem('email');
    this.couples = service.getCartItems();
    this.couples.forEach((element: any) => {
      this.total = this.total + parseInt(element.price);
    });
  }
  ngOnInit() {

  }
  deleteCartEvent(couple: any) {
    const i = this.couples.findIndex((element: any) => {
      return element.id == couple.id;
    });
    this.couples.splice(i, 1);
    this.total = this.total - couple.price;
  }
  purchase() {
    this.couples = [];
    this.total = 0;
    this.service.setCartItems();
  }
}