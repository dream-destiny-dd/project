import { Component } from '@angular/core';

@Component({
  selector: 'app-cultures',
  templateUrl: './cultures.component.html',
  styleUrl: './cultures.component.css'
})
export class CulturesComponent {
  imageUrl = 'assets/images/darjeeling.jpg';
}
