import { Component , OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
// import { GoogleSignInService } from '../google-sign-in.service';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit{

  siteKey: string = '6LcVAGgpAAAAADQhiqlMJk7-mGv-eBqLDx2D4rAo'; // Add your reCAPTCHA site key here
  aFormGroup! : FormGroup;
  formData:any={
  emailId: '',
  password: '',
  loginForm: '',
}
  user: any;
  

  constructor(private router:Router,private service:UserService) {
  }

   ngOnInit(){
    this.aFormGroup = new FormGroup({
      recaptcha: new FormControl('', Validators.required)
    });
  }

  
  

  async loginSubmit(formData: any) {
    const emailId=formData.emailId;
    const password = formData.password;
    // console.log(formData);

    if(formData.emailId == "admin@gmail.com" && formData.password == "Admin@7J") {
      alert("Login Success!!!");
      this.service.setIsUserLoggedIn();
      this.router.navigate(['home']);
    } else {
      this.user= null;

    await this.service.userLogin(emailId, password).then((data: any) => {
      console.log(data);
       this.user = data;
    });
  
    if (this.user != null) {
      this.service.setIsUserLoggedIn();        
      this.router.navigate(['home']);
     
    } else {
      alert('Invalid Credentials');
    }
  }
}
}
