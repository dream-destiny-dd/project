import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SignupComponent } from './signup/signup.component';
import { SigninComponent } from './signin/signin.component';
import { authGuard } from './auth.guard';
import { AboutComponent } from './about/about.component';
import { SignoutComponent } from './signout/signout.component';
import { PlacesComponent } from './places/places.component';
import { FriendsComponent } from './friends/friends.component';
import { CoupleComponent } from './couple/couple.component';
import { FamilyComponent } from './family/family.component';
import { CartComponent } from './cart/cart.component';

const routes: Routes = [
  {path:"home",          component:HomeComponent},
  {path:"places",        component:PlacesComponent},
  {path:"friends",       component:FriendsComponent},
  {path:"couple",        component:CoupleComponent},
  {path:"family",        component:FamilyComponent},
  {path:"signup",        component:SignupComponent},
  {path:"signin",        component:SigninComponent},
  {path:"signout",       canActivate:[authGuard], component:SignoutComponent},
  {path:"cart",          component:CartComponent},
  {path:"aboutus",       component:AboutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }