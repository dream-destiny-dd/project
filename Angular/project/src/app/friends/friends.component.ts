import { Component } from '@angular/core';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrl: './friends.component.css'
})
export class FriendsComponent {

  friends:any;
  
  constructor() {
  this.friends = [
    {
      id: 1000,
      name: "GOA",
      flipname: "Where Every Moment is an Epic Adventure with Friends",
      description: "Goa, a coastal paradise on the western shores of India, is synonymous with sun, sand, and endless fun, making it the ultimate destination for unforgettable adventures with friends. From vibrant beaches to lively nightlife,",
      imgsrc: "assets/images/goa.jpg"
    },

    { 
      id: 1001,
      name: "PONDICHERRY", 
      flipname: "Unforgettable Moments, Endless Memories with Friends",
      description: "Pondicherry, a charming coastal town on the southeastern coast of India, beckons friends with its unique blend of French colonial heritage, vibrant culture, and serene beaches. Pondicherry offers the perfect setting for friends to unwind, explore, and create unforgettable memories together.",
      imgsrc: "assets/images/pondicherry.webp" 
    },

    { 
      id: 1002, 
      name: "KERALA", 
      flipname: "God's Own Country",
      description: "From exploring misty hill stations to cruising through tranquil backwaters and indulging in mouthwatering cuisine, Kerala offers friends the perfect setting to bond, explore, and create cherished memories together.",        
      imgsrc: "assets/images/kerala.webp" 
    },

    { 
      id: 1003, 
      name: "HYDERABAD", 
      flipname:"Where biryani bonds people", 
      description: " At the heart of the city lies the iconic Charminar, a symbol of Hyderabad's rich heritage and architectural splendor. Surrounding this historic monument, the bustling lanes of the old city beckon with their colorful markets, where friends can immerse themselves in the sights, sounds, and aromas of traditional bazaars.",        
      imgsrc: "assets/images/hyd.webp" 
    },

    { 
      id: 1004, 
      name: "GOKARNA", 
      flipname: "Where Every Wave is a Melody of Friendship",
      description: "Nestled along the picturesque coastline of Karnataka, Gokarna is a hidden gem that beckons friends with its pristine beaches, rugged cliffs, and laid-back vibe. From beach hopping to water sports and beachside bonfires.",        
      imgsrc: "assets/images/gokarna.webp" 
    },

    { 
      id: 1005, 
      name: "RISHIKESH", 
      flipname:"Where Every Beat of Adventure Echoes Friendship",
      description: " From thrilling river rafting to serene yoga sessions and vibrant street markets, Rishikesh offers friends the perfect blend of adventure, spirituality, and camaraderie. For the thrill-seekers, Rishikesh is a paradise of adventure activities waiting to be explored. ",        
      imgsrc: "assets/images/rishikesh.jpg" 
    },

    { 
      id: 1006, 
      name: "RAJASTHAN", 
      flipname:"Where Every Sunset Paints a Canvas of Friendship",
      description: "Rajasthan, the Land of Kings, is a vibrant tapestry of colors, cultures, and majestic landscapes that beckons friends with its regal charm, timeless heritage, and warm hospitality. From exploring ancient forts and palaces to camel safaris in the desert and vibrant bazaars.",
      imgsrc: "assets/images/rajasthan.jpg" 
    },

    { 
      id: 1007, 
      name: "LADAKH",
      flipname:"Where Friendship Reaches New Heights", 
      description: "Embark on a thrilling road trip along the scenic highways of Ladakh, where friends can traverse winding mountain roads, cross high-altitude passes, and marvel at breathtaking vistas of snow-capped peaks and azure lakes. ",        
      imgsrc: "assets/images/ladakh.jpg" 
    },

    { 
      id: 1008, 
      name: "SIKKIM", 
      flipname:"Where Nature's Symphony Serenades the Soul", 
      description: "Indulge in the flavors of Sikkimese cuisine, with its unique blend of Tibetan, Nepali, and Bhutanese influences. Share meals of momos, thukpa, and gundruk soup at local eateries and homestays, savoring the rich flavors and spices of traditional Sikkimese dishes.",        
      imgsrc: "assets/images/sikkim.jpg" 
    },

    { 
      id: 1009, 
      name: "WEST BENGAL", 
      flipname:"Where Tradition Meets Modernity", 
      description: "Experience the vibrant culture and traditions of West Bengal by attending colorful festivals and cultural events. Join locals in celebrating festivals like Durga Puja, Diwali, and Holi, where streets come alive with music, dance, and elaborate decorations.",        
      imgsrc: "assets/images/westbengal.webp"
    },

    { 
      id: 1010, 
      name: "MEGHALAYA", 
      flipname:"Where Friendship Blooms Amid Nature's Embrace", 
      description: " From exploring ancient caves to wandering through lush rainforests and soaking in the beauty of cascading waterfalls, Meghalaya promises friends an unforgettable journey of discovery and camaraderie.",        
      imgsrc: "assets/images/meghalaya.avif" 
    },

    
  ];


}
}
