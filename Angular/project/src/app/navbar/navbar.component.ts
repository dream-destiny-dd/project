import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Route } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent implements OnInit{
  
  loginStatus: any;

  constructor(private service: UserService) {
  }

  ngOnInit() {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });
  }
  
    // getLoginStatus(): boolean {
    //   return this.userService.getLoginStatus();
    // }
    // getUserName(): string {
    //   const userData = localStorage.getItem("user");
    //   if (userData) {
    //     const user = JSON.parse(userData);
    //     return user.name; // Assuming the user object has a 'name' property
    //   }
    //   return "";
    // }
  
    // logout() {
    //   localStorage.removeItem("user"); 
    //   this.userService.setUserLogOut();
    //   this.router.navigate(['/signin']); 
      // this.toastr.info("Logged out successfully");
      // Other logout logic
    // }
  }