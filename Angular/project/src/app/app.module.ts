import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { AboutComponent } from './about/about.component';
import { SignoutComponent } from './signout/signout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxCaptchaModule } from 'ngx-captcha';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PlacesComponent } from './places/places.component';
import { CulturesComponent } from './cultures/cultures.component';
import { FriendsComponent } from './friends/friends.component';
import { CoupleComponent } from './couple/couple.component';
import { FamilyComponent } from './family/family.component';
import { KaaliComponent } from './kaali/kaali.component';
import { PondicherryComponent } from './pondicherry/pondicherry.component';
import { AdiyogiComponent } from './adiyogi/adiyogi.component';
import { CartComponent } from './cart/cart.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    SigninComponent,
    SignupComponent,
    AboutComponent,
    SignoutComponent,
    PlacesComponent,
    CulturesComponent,
    FriendsComponent,
    CoupleComponent,
    FamilyComponent,
    KaaliComponent,
    PondicherryComponent,
    AdiyogiComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxCaptchaModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
